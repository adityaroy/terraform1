variable "vpc_cidr" {
    default = "10.0.0.0/16"
  
}
variable "tenancy" {
    default = "default"
  
}

variable "public_sub_cidr" {
    default = "10.0.1.0/24"
  
}
variable "private_sub_cidr" {
    default = "10.0.2.0/24"
  
}
