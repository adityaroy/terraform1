resource "aws_vpc" "vpc1" {
  cidr_block       = "${var.vpc_cidr}"
  instance_tenancy = "${var.tenancy}"

  tags = {
    Name = "mainvpc"
  }
}


resource "aws_subnet" "public" {
  vpc_id     = "${aws_vpc.vpc1.id}"
  cidr_block = "${var.public_sub_cidr}"
  map_public_ip_on_launch = true

  tags = {
    Name = "public"
  }
}
resource "aws_subnet" "private" {
  vpc_id     = "${aws_vpc.vpc1.id}"
  cidr_block = "${var.private_sub_cidr}"

  tags = {
    Name = "private"
  }
}

resource "aws_internet_gateway" "internetgateway" {
  vpc_id = "${aws_vpc.vpc1.id}"

  tags = {
    Name = "mygateway"
  }
}



resource "aws_security_group" "sg_for_pub_subnet" {
name = "terraformsecurity"
description = "use this SG for pub subnet instance"

vpc_id = "${aws_vpc.vpc1.id}"

ingress{
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

}
ingress{
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

}
ingress{
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

}
}

resource "aws_security_group" "sg_for_pri_subnet" {
name = "terraformsecurity"
vpc_id = "${aws_vpc.vpc1.id}"
description = "SG for private instance to connect from public ins"
ingress{
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${var.public_sub_cidr}"]

}
ingress{
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${var.public_sub_cidr}"]

}
ingress{
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.public_sub_cidr}"]

}
ingress{
    from_port   = 0
    to_port     = 65535
    protocol    = "icmp"
    cidr_blocks = ["${var.public_sub_cidr}"]
  }
}
resource "aws_eip" "natip" {
  tags {
    Name = "natIP"
  }
}

resource "aws_nat_gateway" "nat" {
  allocation_id = "${aws_eip.natip.id}"
  subnet_id     = "${aws_subnet.public.id}"

  tags = {
    Name = "nat gw"
  }
}


resource "aws_route_table" "routeout" {
  vpc_id = "${aws_vpc.vpc1.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.internetgateway.id}"
  }
  route {
    ipv6_cidr_block        = "::/0"
    egress_only_gateway_id = "${aws_internet_gateway.internetgateway.id}"
  }
    tags {
      Name = "public_subnet_route"
    }
}

  resource "aws_route_table_association" "public_subnet" {
  subnet_id      = "${aws_subnet.public.id}"
  route_table_id = "${aws_route_table.routeout.id}"
}

resource "aws_route_table" "privateroute" {
  vpc_id = "${aws_vpc.vpc1.id}"

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.nat.id}"
  }
    tags {
      Name = "privaterout"
    }
  }

  resource "aws_route_table_association" "private_subnet" {
  subnet_id      = "${aws_subnet.private.id}"
  route_table_id = "${aws_route_table.privateroute.id}"
}
